console.log("Hello world");

/* 
Functions in javascript are lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked. 

functions are mostly created to create complicated tasks to run several lines of code in succession.

they are used to prevent repeating lines/blockes of codes with same function
*/


// function declaration

function printName(){
    console.log("My name is John");
}

// call a function/invoke a function
/* 
the code block and statements inside a function is not immediately executed when the function is defined/declared. The code block/statements inside a function is executed when the funciton is invoked/called.
*/
printName();


/* // results in an error due to non-existence of the function (not declared)

declaredFunction (); */


// DECLARATION VS EXPRESSION
// declaration - using 'function' and functionName
// functions can be hoisted

declaredFunction (); 

/* 
functions in js can be used before it can be defined, thus hoisting is allowed for the javascript functions

    NOTE: hoisting is javascripts default behavior for certain variable and functions to run/use them before declaring them
*/

function declaredFunction(){
    console.log("Hello from declaredFunction");
};

// FUNCTION EXPRESSION
/* 
    a function can also be created by storing it in a variable

    cannot be called before declaration

    does not allow hoisting

    a function expression is an anonymous function assigned to variableFunction
        anonymous function - unnamed function
*/
// variableFunction(); - calling a function that is created through an expression before it can be defined will result in an error
let variableFunction = function(){
    console.log("Hello again!");
}

variableFunction();

/* 
miniactivity 
    create two functions
        1st function - log in console the top three anime you would like to recommend
        2nd function - log in the console the top 3 movies you would like to recomnend
*/

function topAnime(){
    console.log("Watch these animes: Slam Dunk, Naruto, and One Piece");
};

topAnime();

function topFilms(){
    console.log("My top 3 movie recommendations are: Walang Forever, Fight Club, and Django");
};

topFilms();

// Reassigning declared functions
// We can reassign declared functions and function expressions to new anonymous functions

declaredFunction = function(){
    console.log("updated declarationFunction");
};

declaredFunction();

// However, we cannot change the declared functions/function expressions that are defined using const

const constantFunction = function(){
    console.log("Initialized const function");
};

constantFunction();

// constantFunction = function(){
//     console.log("cannot be re-assigned");
// };

// constFunction();

// Function scoping
/* 
scope is the accessibility of variables/functions

JS variables /functions have 3 scopes
    1. local/block scope - can be accessed inside the curly brace/code block {}

    2. global scope - anything that is written in the .js file / outer most part of the codes - can be called inside a code block
*/


{
    let localVar = "Armando Perez";
    console.log(localVar);
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);
// console.log(localVar);
/* 
    Function scope - can only work if called inside the function
    variables define inside a function can only be used and accessed inside that funciton
    variables declared with var let and const 
*/

function showNames(){
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}
showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

/* 
functionVar, functionConst, and functionLet are function-scoped and cannot be accessed outside the function they are declared in
*/


// Nested Function - declared inside another function. These nested functions have function scope only inside the block where they were defined
function newFunction(){
    let name ="Jane";
    
    // nested function
    function nestedFunction(){
    let nestedName = "John";
    // valide since we are accessing it inside a nested function that is still inside the function where name is
    console.log(name);
    console.log(nestedName);
};
    // console.log(nestedName); - returns an error since the nestedName has function scoping, it can only be accessed inside the nestedFunction
    nestedFunction();
    };

    newFunction();
    // nestedFunction(); - returns an error since we must call the nestedFunction inside the funciton where it is declared

    /* 
    miniactivity
    create a global scoped variable
    create a function
        create another let variable
        log in the console the two variables
    call the function
    */

    let weAre = "Amy";

    function letsGo (){
        let underStand = "Jamie";
        console.log(weAre);
        console.log(underStand);
    };

    letsGo();


    // Using alert()
    // alert allows us to show a small window at the top of our browser to show information to our users, as opposed to a console.log() which only shows the message on the console. It allows us to show a short dialogue  or instructions to our user. The page will wait / continue to load until user closes dialog 

    alert("Hello World");

    function showSampleAlert(){
        alert("Hello Again!");
    }

    showSampleAlert();
    // We will find that the page waits for the user to dismiss the dialog box before proceeding. We can witness this by reloading the page while the console is open
    console.log("I will be displayed after the alert has been closed.");

    /* 
    shown only an alert() for short dialogs/messages to the user
    do not overuse alert() because the program/js has to wait for it to be dismissed before continuing
    */

    // Using prompt()
    // prompt() - used to allows us to show a small window at the top of the browser to gather user input. Much like alert(), it will have the page wait until the user completes or enters the input. The input from the prompt() will be returned as a string data type once the user dismisses the window. 

    /* 
    Syntax
    prompt("<dialogInString>");
    */

    // let samplePrompt = prompt("Enter your name.");
    // console.log("Hello" + samplePrompt);


    // let nullPrompt = prompt("do not enter anything here");

    // console.log(nullPrompt);

    // prompt() returns an empty string if the user clicks OK button without entering any information and null for users who click the CANCEL button in the window

    /* 
    NOTES ON USING prompt()
    It can be used to gather user input and be used in our code. However, since it will have the page wait until the user finished or closed the window. It must not be overused. 
    */

    function welcomeMessage(){
        let firstName = prompt("Enter your first name");
        let lastName = prompt("Enter your last name");

        console.log("Hello " + firstName + " " + lastName);
    };

    welcomeMessage();


    // Function Naming
    // should be definitive of the task it will perform, usually starts with verb/adjective

    // 1. definitive task of it will perform
    // 2. name your functions with small letters; in cases of multiple words, use camelCasing (underscore is also valid)

    function getCourses(){
        let courses = ["Science 101", "Math 101", "English 101"];
        console.log(courses);
    };
    getCourses();

    function get(){
        let name = "Jamie";
        console.log(name);
    };

    get();

    // avoid pointless and inappropriate name for our functions; foo is used as a placeholder in stackoverflow solutions
    function foo(){
        console.log(25%5);
    };

    foo();