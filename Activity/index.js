/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
function userInfo(){
   let fullName = prompt("Enter your Full Name:");
   let userAge = prompt("Enter your Age:");
   let userLocation = prompt("Enter your Location:");

   alert("Thank you for your input!")

    console.log("Hello, " + fullName);
    console.log("You are " + userAge + " years old.");
    console.log("You live in " + userLocation);
}

userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function topArtists(){
    console.log("1. Taylor Swift");
    console.log("2. Harry Styles");
    console.log("3. Miley Cyrus");
    console.log("4. Tyler the Creator");
    console.log("5. Lorde");
};

topArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
function topFilms(){
    console.log("1. Beautiful Boy");
    console.log("Rotten Tomatoes Rating: 67%");
    console.log("2. Django");
    console.log("Rotten Tomatoes Rating: 86%");
    console.log("3. Fight Club");
    console.log("Rotten Tomatoes Rating: 79%");
    console.log("4. Everything Everywhere All at Once");
    console.log("Rotten Tomatoes Rating: 95%");
    console.log("5. The Danish Girl");
    console.log("Rotten Tomatoes Rating: 66%");
};

topFilms();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// function printUsers(){
// let printFriends = 

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
// };

printUsers();


// console.log(friend1);
// console.log(friend2);